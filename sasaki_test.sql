-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 01, 2013 at 10:02 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sasaki_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `UID` bigint(20) NOT NULL,
  `ProjectName` varchar(40) NOT NULL,
  `ProjectYear` bigint(20) NOT NULL,
  `Tags` varchar(25) NOT NULL,
  `Description` varchar(283) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`UID`, `ProjectName`, `ProjectYear`, `Tags`, `Description`) VALUES
(1, 'Ohio State', 2009, ',0,1,20,', 'Recipient of the 2011 SCUP Award for Institutional Innovation and Integration, the One Ohio State Framework Plan redefines campus master planning.'),
(2, '2008 Beijing Olympics', 2007, ',3,4,5,6,7,2,8,', 'Sasaki provided planning and urban design for the Olympic Green—the principal venue of the 2008 Beijing Olympics'),
(3, 'Wilmington Waterfront Park', 2003, ',9,6,10,11,2,8,12,', 'Once a part of the Pacific coastline, the Wilmington community became disconnected from the waterfront by the Port of Los Angeles - a burgeoning, diverse mix of industrial maritime facilities'),
(4, 'Cleveland Euclid Avenue Healthline BRT', 2004, ',13,14,11,15,16,', 'Cleveland''s Euclid Avenue is being transformed by a strategic $200 million investment in a Bus Rapid Transit Corridor, which has catalyzed $5.8 billion dollars in spin-off investments and over 13.5 million square feet of new development.'),
(5, 'Charleston Waterfront Park', 2007, ',9,17,5,6,10,2,8,12,18,', 'Working with city leadership for multiple decades, Sasaki has helped strengthen the public realm of Charleston, South Carolina through a series of planning, landscape, and architectural initiatives.'),
(6, 'Waterway Square', 2003, ',9,6,19,14,2,12,18,', 'In 2003, Sasaki was selected to reposition the automobile-oriented arrangement of office buildings and indoor mall into a new downtown. Sasaki created a Town Center Master Plan, which organizes a variety of pedestrian-oriented uses along a network of streets and new urban spaces. ');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `UID` bigint(20) NOT NULL,
  `Name` varchar(27) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`UID`, `Name`) VALUES
(0, 'Campus'),
(1, 'Architecture'),
(2, 'Urban Planning and Design'),
(3, 'District Plan'),
(4, 'Landscape Master Plan'),
(5, 'Legacy'),
(6, 'Park'),
(7, 'Sports'),
(8, 'Urban Regeneration'),
(9, 'Civic Space'),
(10, 'Stormwater Management'),
(11, 'Sustainability'),
(12, 'Water Feature'),
(13, 'Environmental Graphics'),
(14, 'Streetscape'),
(15, 'Transit'),
(16, 'Transportation Planning'),
(17, 'Ecological Restoration'),
(18, 'Waterfront'),
(19, 'Plaza'),
(20, 'Planning');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
