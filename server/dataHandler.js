//region node.js core
var url = require('url');
//endregion
//region npm modules
var mysql = require('mysql');
//endregion
//region modules

//endregion

/**
 @class DataHandler
 */
var DataHandler = function () {
    var _self = this;
    var _connectionInfo = {
        host: process.env.IP,
        user: process.env.C9_USER,
        password: '',
        database: 'c9'
    };
    //region private fields and methods
    var _runQuery = function (query, callback) {
        var connection = mysql.createConnection(_connectionInfo);
        connection.connect();
        connection.query(query, function (err, rows, fields) {
            if (err) throw err;
            if (callback) callback(rows);
        });

        connection.end();
    };

    var _returnJsonObj = function (res, obj) {
        //use the standard response object to return data to the client as JSON.
        res.writeHeader(200, {"Content-Type": "application/json"});
        res.write(JSON.stringify(obj));
        res.end();
    };

    var _parseUrlGetVars = function (req) {//--parse the GET variables part of the URL
        var url_parts = url.parse(req.url, true);
        return url_parts.query;
    };
    //endregion

    //region public API

    this.getNames = function (req, res) {
        //--call our query function and pass in a callback function to use when it successfully gets rows for the query
        _runQuery('SELECT `UID`, `ProjectName` FROM `projects`', function (rows) {
            var projects = [];
            rows.forEach(function (row) {//each row object contains the SQL field names as properties
                projects.push({id: row.UID, value: row.ProjectName});//we make an array of new objects with an id and value to return to the client
            });
            _returnJsonObj(res, projects);
        });
    };

    this.getContent = function (req, res) {
        var id = _parseUrlGetVars(req).id;//get the id that we passed in from the client side as a GET variable: e.g. getContent?id=4
        _runQuery('SELECT `ProjectName`, `Description` FROM `projects` WHERE `UID` = ' + id, function (rows) {
            if (rows.length == 0) {
                _returnJsonObj(res, {error: "No Results Found"});
            }
            if (rows.length > 1) {
                _returnJsonObj(res, {error: "Multiple results!"});//--should never happen with unique ID
            }
            var row = rows[0];
            _returnJsonObj(res, {title: row.ProjectName, content: row.Description});
        });
    };
    //endregion
};

module.exports.DataHandler = DataHandler;

