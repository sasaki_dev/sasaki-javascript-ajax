//region nodejs core
var http = require("http");// load references to standard node modules
var url = require("url");
var path = require('path');
//endregion
//region npm modules
var nodeStatic = require('node-static');//load a reference to the node-static module we added via npm
//endregion
//region modules
var dh = require('./server/dataHandler');//load a reference to our own dataHandler module
//endregion

var file = new (nodeStatic.Server)(path.resolve(__dirname, "client"));//--set up the node-static server to serve static files (e.g. html and script files) from our 'client' folder
var dataHandler = new dh.DataHandler();//--create an instance of our DataHandler class

var handle = {};//make an object that defines how certain URL paths are directed to methods on various objects.
handle["/getNames"] = dataHandler.getNames;
handle["/getContent"] = dataHandler.getContent;

var onRequest = function (req, res) {
    var pathname = url.parse(req.url).pathname;
    console.log('Processing request for path: '+pathname);

    req.addListener("end", function () {
        //console.log("About to route a request for " + pathname+ " : "+action);
        if (typeof handle[pathname] === 'function') {
            handle[pathname](req, res);
        } else {
            //if there are not any specific functions assigned to this URL path, then we serve up the static files (such as html files and images) directly using the node-static module
            file.serve(req, res);
        }
    }).resume();
};

var app = http.createServer(onRequest);
app.listen(process.env.PORT, process.env.IP);
