var _addNameAsSelectOption = function (nameObj) {
    //TODO --Hint: look at the jquery examples file to see how to add a select option to the list
};

var _makeResultCard = function (name, content) {
    //TODO --Hint: look at the jquery examples file
};

var _getContent = function (selVal) {
    //--call the server to get the content for the selected element
    $.get('/getContent?id=' + selVal, function (data) {
        if (data.error) {//--if there was an error, let the user know
            alert(data.error);
        } else {//--create a card with the title and content from the server
            _makeResultCard(data.title, data.content);
        }
    });
};

$(document).ready(function () {
    //when the document has loaded, we need to ask the server for a list of names and then populate the select list control
    $.get('/getNames', function (data) {
        //this code is run only once - when the document loads
        //data is an array of objects containing an id and value - we loop through these and add them to the select list
        $.each(data, function (i, nameObj) {//--this is the jQuery way of writing a for each loop
            _addNameAsSelectOption(nameObj);
        });
    });

    var $selName = $('#selName');//store a reference to the HTML element whose ID is 'selName';
    $selName.change(function () {
        //run this code whenever the user picks a new item from the select list
        var selVal = $(this).val();//get the current value for "selName" - the 'this' keyword can be confusing in javascript, but within a jQuery callback it usually refers to the object that has been clicked or selected
        _getContent(selVal);
    });
});